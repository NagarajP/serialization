import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/*
 * Difference between static and transient.
 * As we know, transient is used where the state of the variable in instance should not be saved.
 * if the variable is static, then its state will be at class level. So its also not available for 
 * serialization. Ex: Student.name
 * But in deserialization, transient variable value will get default value and static variable 
 * value will try to check any default value available at class level, if its available then it will
 * assign the same.
 */
public class SerializationDemo {

	public static String fileName = "student.txt";

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Student s = new Student();
		// ((Student) p).StudentMethod();
		// System.out.println(p.gender + " in main");
		 s.studName = "nagaraj";
		try {

			/*
			 * Serialization
			 */
			FileOutputStream fos = new FileOutputStream(fileName);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			// while serializing student object, even all the parents class data
			// members also will be serialize,
			// This is one disadvantage.
			oos.writeObject(s);

			/*
			 * Deserialization
			 */

			FileInputStream fis = new FileInputStream(fileName);
			ObjectInputStream ois = new ObjectInputStream(fis);
			Student st = (Student) ois.readObject();
			System.out.println("After de-serialization");
			System.out.println(" name - " + Student.studName);
			System.out.println(" gender - " + st.gender);
			System.out.println(" disability - " + st.disability);
			System.out.println(" roll - " + st.roll);
			System.out.println(" profession - " + st.profession);
			System.out.println(" email - " + st.email);

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}

//class Person implements Serializable{
class Person {
	public String gender = "male";
	public String disability = "No";
	public transient String profession = "se";	// even its transient variable, it will serialize the value
												// value unless Person class is implemented Serializable interface.

	public void PersonMethod() {
		System.out.println("in personmethod");
	}
}

class Student extends Person implements Serializable {
	
	private static final long serialVersionUID = 1L;
	public static String studName;
	public transient int roll = 121;
	public transient String email = "a@gmail.com";
//	public static final int id;		// final value should be initialized during declaration 
									// or in any static method(if it is 'final static' variable) 
									// or in constructor.
	/*
	static {
		id = 99;
	}
	*/

	public void StudentMethod() {
		System.out.println("In Student method");
		System.out.println(gender);
	}
}