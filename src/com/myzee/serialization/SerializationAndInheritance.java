package com.myzee.serialization;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class SerializationAndInheritance {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		// TODO Auto-generated method stub
		System.out.println("Serializing object");
		FileOutputStream fos = new FileOutputStream(new File("emp.txt"));
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		Address addr = new Address("Bengaluru", 580040);
		Employee emp = new Employee(1, "nagaraj", addr);
		oos.writeObject(emp);
		
		System.out.println("deserializing object");
		FileInputStream fis = new FileInputStream("emp.txt");
		ObjectInputStream ois = new ObjectInputStream(fis);
		Employee e = (Employee)ois.readObject();
		System.out.println(e);
		
		//	Inheritance
		Worker w = new Worker(33, "test worker");
		fos = new FileOutputStream(new File("worker.txt"));
		oos = new ObjectOutputStream(fos);
		oos.writeObject(w);
		
		fis = new FileInputStream("worker.txt");
		ois = new ObjectInputStream(fis);
		Worker w1 = (Worker)ois.readObject();
		System.out.println(w1);
		
	}

}

class Address implements Serializable{		// must be serialized to use it inside employee.
	private String city;
	private int pin;
	public Address(String city, int pin) {
		this.city = city;
		this.pin = pin;
	}
	
	@Override
	public String toString() {
		return "city:" + city + "\npin:" + pin;
	}
}

class Employee implements Serializable {
	
	protected int id;
	protected String name;
	protected Address addr;
	public Employee() {}
	public Employee(int id, String name, Address addr) {
		this.id = id;
		this.name = name;
		this.addr = addr;
	}
	
	@Override
	public String toString() {
		return "id:" + id + "\n name:" + name + "\n address:" + addr;
	}
}

class Worker extends Employee {
	private int worker_id;
	private String worker_name;
	public Worker(int id, String name){
		this.worker_id = id;
		this.worker_name = name;
	}
	
	@Override
	public String toString() {
		return "worker id:" + worker_id + "\n worker name:" + worker_name + 
				"\n employee id:" + id + "\n emp name:" + name +
				"\n emp address" + addr.toString();
	}
	
}
