package com.myzee.serialization;

/*
 * Note: if you serializing child object, then it will serialize parent field data also.
 * even if you apply transient on parent field, it work.
 * if transient to work on parent field, you should implement Serializable on parent also
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class SerializeAndExtends {

	public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {
		// TODO Auto-generated method stub
		ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(new File("aa.txt")));
		oos.writeObject(new Employees("abc", 10000));
		
		ObjectInputStream ois = new ObjectInputStream(new FileInputStream("aa.txt"));
		Employees e = (Employees)ois.readObject();
		System.out.println(e);
		
		
		// test, if super class serializable, child automatically serializable?
		System.out.println("test case scenario\n");
		scenario();
	}

	/**
	 * 
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * Desc: if super class is serialized, then derived class is automatically serialized
	 */
	private static void scenario() throws FileNotFoundException, IOException, ClassNotFoundException {
		ObjectOutputStream oos  = new ObjectOutputStream(new FileOutputStream(new File("institute.txt")));
		Student s = new Student();
		oos.writeObject(s);
		
		ObjectInputStream ois = new ObjectInputStream(new FileInputStream("institute.txt"));
		Student s1 = (Student) ois.readObject();
		System.out.println(s1);
	}

}

//class Company implements Serializable {	//now transient on below field will work
class Company {
	transient String compName = "lear";
	int compManPower = 100000;
	
}

class Employees extends Company implements Serializable{
	String empName;
	transient int empSalary;
	
	/**
	 * @param empName
	 * @param empSalary
	 */
	public Employees(String empName, int empSalary) {
		super();
		this.empName = empName;
		this.empSalary = empSalary;
	}

	@Override
	public String toString() {
		return "Employees [empName=" + empName + ", empSalary=" + empSalary + ", compName=" + compName
				+ ", compManPower=" + compManPower + "]";
	}
}

class Institute implements Serializable {
	String instituteName = "TCE";
	String instituteId = "2TG";
}

class Student extends Institute{
	String studentName = "nagaraj";
	transient int studentId = 024;
	@Override
	public String toString() {
		return "Student [studentName=" + studentName + ", studentId=" + studentId + ", instituteName=" + instituteName
				+ ", instituteId=" + instituteId + ", \ngetClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}
	
}
